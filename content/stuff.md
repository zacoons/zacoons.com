---
title: stuff
description: A collection of cool sites, people, books, and wisdom.
---

## Software I use

### Video Editing

OBS for recording and Kdenlive for editing. I have tried many many video editors, but Kdenlive is my favourite so far.
I previously used DaVinci Resolve, but I'd prefer to use FOSS even if it's not as high quality.

### IDE

NeoVim.

### Browser and Search

Mullvad Browser on desktop, Tor Browser on mobile. For my search engine I use SearXNG (successor of SearX).

### OS

Arch Linux on desktop, Calyx on mobile. I'd prefer to be using Artix on desktop but I can't be bothered switching atm.

### Passwords and 2FA

KeePass. The GUIs I use are KeePassXC for desktop and KeePassDX for mobile.

### Portfolio

For keeping track of my investments I use Portfolio Performance. For crypto I just use the official Monero wallet.

### Messaging

For Matrix I use SchildiChat.\
For XMPP I use Dino on desktop and Conversations on mobile.\
For email I use Evolution on desktop and K-9 Email on mobile.

## Sites

- [My old site](https://old.zacoons.com)
- [Comfy.Guide](https://comfy.guide)
- [Retro GIFs](https://gifcities.org)
- [Search engine for retro sites](https://wiby.me)
- [Windows 98 icon viewer](https://win98icons.alexmeub.com/)

## People

- [This guy](https://www.youtube.com/@newbeithbaptistchurch965/streams) (great sermons)
- [Mental Outlaw](https://www.youtube.com/@MentalOutlaw) (internet landchad)
- [Luke Smith](https://lukesmith.xyz) (internet landchad)
- [Denshi](https://denshi.org) (internet landchad)
- [IMBeggar](https://www.youtube.com/@imbeggar) (christian stuff)
- [Canon press](https://www.youtube.com/@CanonPress) (christian stuff)
- [Jay Dyer](https://www.youtube.com/@JayDyer) (based christian and philosopher)

## Books

- <cite>Future Men: Raising Boys to Fight Giants</cite> by Douglas Wilson
- <cite>It's Good to be a Man: A Handbook for Godly Masculinity</cite> by Dominic Bnonn Tennant & Michael Foster
- <cite>Beginning to Pray</cite> by Anthony Bloom

## Wisdom

> "Those who do not have Christ in them are full of anxiety. But when man grasps the deepest meaning of life, then all his anxiety goes away... Do not worry about anything. Anxiety is the devil. When you see anxiety, know that it's the devil's work."\
<cite>-- St. Paisios the Athonite</cite>

---

> "Get your finances in order and work on your goals - be ambitious and driven. Learn to lead, beginning with family like your mother and sister, or with friends by planning events, meet-ups, family dinners etc and start to take decisions. Address any personal traumas and work on healing. Improve your flirting and communication skills. Always be authentic and unapologetically yourself. Process your emotions, journaling would be a great idea, surround yourself with mentors and people who align with your goals and values. Embrace your shortcomings and fears and accept the things which you cannot change. Set values, standards, and qualities for yourself. Seek a woman who aligns with these values. Always be a chooser, not a chaser. Always qualify. Understand female nature, their needs, and your own needs. That's it."\
<cite>-- Some guy</cite>

---

> "It is a sad fate for a man to die too well known to everybody else, and still unknown to himself."\
<cite>-- Francis Bacon</cite>

---

> "Nothing happens to anybody which he is not fitted by nature to bear."\
<cite>-- Marcus Aurelius</cite>

---

> "Death smiles at us all, but all a man can do is smile back."\
<cite>-- Marcus Aurelius</cite>

---

> "What we do now echoes in eternity."\
<cite>-- Marcus Aurelius</cite>

---

> "Waste no more time arguing about what a good man should be. Be one."\
<cite>-- Marcus Aurelius</cite>

---

> "'Tis no Sin to be tempted, but to be overcome."\
<cite>-- William Penn</cite>

---

> "He who fears he shall suffer, already suffers what he fears."\
<cite>-- Michel de Montaigne</cite>

---

> "I prefer the company of peasants because they have not been educated sufficiently to reason incorrectly."\
<cite>-- Michel de Montaigne</cite>

---

> "On the highest throne in the world, we still sit only on our own bottom."\
<cite>-- Michel de Montaigne</cite>
