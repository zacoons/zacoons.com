---
title: projects
description: What I'm up to and stuff I've done.
---

## Active

{{<randomFace>}}

## Completed

[WebGPU TurboWarp Extension](https://github.com/TurboWarp/extensions/pull/1424) <small>2024</small>

I made a video about this. Still hasn't been merged yet, but above is a link to the PR.

[PYSToL browser extension](https://chromewebstore.google.com/detail/purge-youtube-suggestions/jhgijamhkfofiagnfjpaidaffdgbaehm) <small>2024</small>

An epic browser extension that Purges Youtube Suggestions (Totally Legit). It's easy to get lost in the catalogue of interesting videos that youtube feeds you. PYSToL eliminates that temptation.

[AI simley face recogniser](https://machine-learning-js.vercel.app) <small>2023</small>

My first successful AI project. Wrote the algorithms in JS and trained it myself with data I created. Discerns whether your drawing as a smiley face or a frowny face.

[Minecraft Mythical Potato Mod](https://old.zacoons.com/mythicalpotatomod/v2) <small>2021</small>

I've got a couple of videos about it on youtube.

[ZBot the Discord bot](https://old.zacoons.com/discord/zbot) <small>2021</small>

The discord bot in my old server. Back when I used discord.

## Games

[Skuare](/skuare) <small>old, but remade in 2023</small>

A puzzle game. I bet you can't beat size 10.

[itch.io](https://zacoons.itch.io) <small>2018-2021</small>

[Scratch](https://scratch.mit.edu/users/zacharymckenzie) <small>ancient</small>
