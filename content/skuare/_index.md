---
title: skuare
description: A nifty age-old puzzle game where the goal is to turn all of the tiles green. I dare you to beat size 10.
---

## Embed it in your own site

```html
<!-- head -->
<link rel="stylesheet" href="https://zacoons.com/skuare.min.css">

<!-- body -->
<script src="https://zacoons.com/skuare.min.js"></script>
```
