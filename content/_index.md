---
description: A place of awesome epicness that is very awesome. Includes blog and epic coding.
---

# Latest Video

{{<latestVideo>}}

Subscribe or else [he](/he.webp) will get you.

# Bolg

I write stuff sometimes. Mostly about philosophy or tech.

{{<posts 5>}}

[&hookrightarrow; More](/blog)

# Stuff

Matrix: [@yrahcaz:matrix.org](https://matrix.to/#/@yrahcaz:matrix.org)\
Email: [zac@zacoons.com](mailto:zac@zacoons.com?subject=Hello)\
Codeberg: [zacoons](https://codeberg.org/zacoons)

[❤️ donate](/donate)
