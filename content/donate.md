---
title: donate
description: Donate for whatever reason.
---

# Donate

If you feel like donating for whatever reason then I'm happy to get me summa dat drip.

## Monero

Gigachads use Monero:

```
8AF4pXUJiY7KaXVe9ufDR41YVBPGeyPe4bB1RhQnYn9n7HSJsPcnhJkjnc3vR3XaEjH2wRNKmkX2LizRLoNDZV9eLqjKFoU
```

![Monero QR code](/donateqr.webp)

## Libera Pay (fiat)

<script src="https://liberapay.com/zacoons/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/zacoons/donate"><img alt="Liberapay btn" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>
