---
title: The Temptationbox
date: 2024-02-12
---

I recently attended a youth group at one of those big modern churches with blaring music and youthful crowds. I don't endorse these churches but I went along to the youth group because there aren't any other youth groups around. I rarely meet kids my age, so it surprised me to see how often they checked their phones. Even someone who seemed more resistant than others couldn't help but turn it on and quickly flick through the notifications as the "sermon" was starting. She put it away for the rest of the sermon, for which I commend her, but there was still that fleeting submission to the temptation. In fact it was so fleeting that I doubt she could actually read any of the notifications. So then why did she do it? I suspect it was a reflex rather than genuine desire or need.

This led me to assess my own computer habits, which can definitely use some improvement. My job as a developer requires me to be on the computer, so that only leaves weekends for me to be offline. But I also like to make time for my personal projects, so from thenceforth I will try to spend at least one whole day a week not touching the computer.

Nothing is inherently evil, but devices easily become little boxes of temptation. Anyone can jump online and search for porn, or waste hours on social media, or on building their perfect website (guilty as charged), or on something else entirely. On the other hand, devices offer great improvements to productivity and efficiency. They can give you access to vast stores of knowledge and predictive power if you use them wisely.

### Conclusion

Don't be someone who reflexively checks their phone. I encourage you to embark on regular computer fasts. I don't expect anyone to completely cut themselves off from devices. Just challenge yourself. Catch bad habits quickly and try to fight the call of the Temptationbox.

Rule over technology, don't let it rule you.

If you enjoyed this article, you may find this one interesting to think about:
[18 Rules Over Technology](https://denshi.org/blog/18-rules-over-technology/)
