---
title: Queen of Beauty
date: 2024-08-17
---

*Copied from [Denshi](https://denshi.org/blog/queen-of-beauty)*

*This is a text adaptation of a [homily](https://www.youtube.com/watch?v=nfRk1YUeUaw) given in 2011 by Fr. John Hollowell about beauty*.

![Mother Mary, Queen of Beauty](/blog/mary.webp)

School plays are for pansies; – Or so I used to think.\
Art is only for people who can paint; – Or so I used to think.\
I could care less about a well-cooked, home-made meal; – Or so I used to think.

In high school, I loved to tell everyone that I had no artistic ability. It was like a badge of honor: I was proud to not be one of those "artsy" people. I was a math major, an athlete and a realist. I *did* join the show choir one year when I was a Sophomore, but that was only because *Jamie Schroeder*, the best looking girl in school, a senior cheerleader, decided to join the show choir too, so I thought that was my chance. When she graduated, I quit show choir. *And I don't think the choir director minded seeing me go...*

## Beauty Will Save the World

I say all this because one of the most important revelations that I've had in my life happened about 10 years ago. And it was the realization that all people are called to be **artists**. And that yes, even I, am called to be one too. Pope Benedict XVI said several years ago, "Beauty will save the world." Now if that's true, if beauty will save the world, then we should be people that are in the beauty business. We should bring beauty to the words we write, to the food we cook, to the photographs we take, to the dances we dance, to the roles we act, to the songs we sing, to the music we play, to the videos we make, to the programs we use at Mass, to the little things. Even those things that maybe no one else will ever see.

I know many people now would scoff at this. *"How could beauty possibly save the world?"* Mr. O'Malley or Mrs. Jackson must've paid me off to say that. But I want us to think for a minute about what Pope Benedict might've been saying: How could beauty possibly save the world? I think one of the ways is that beautiful things remind us that *we are human*, and that it is a special thing to be human. We live in an era that tells us it's not a big of a deal to be a human person, *but beautiful things remind us that it is*.

People often say, "what's the difference between animals and people?" and that's really a pretty silly question: No cheetah ever sculpted an image of someone that they loved. No aardvark ever painted a picture of the queen of all aardvarks. And no non-human member of the animal kingdom has ever stopped dead in its tracks observing a sunrise or a sunset.

## Beauty is Scorned by the Wicked

![A Soviet living block](/blog/soviet.webp)

We know beauty will save the world, because it's what the devil attacks. *The devil will strike at the offspring and those things that the Blessed Mother will produce*. One of the ways in which we see beauty attacked in our own world is if we look at the regimes dominated by Communism. The very first thing that Communist leaders attack when they take over a place is beauty. Maybe you've see pictures of Russia, or other places that are ruled by Communist leaders, the architecture is bland, there are no songs other than military songs for parades, buildings have no windows, and everything is bleak and gray and gross.

Beauty reminds us that we are human, and that it means something to be human. Maybe you've seen the movie *The Shawshank Redemption*, where a bunch of prisoners are milling around outside during their time off, ground-down by dirt and dust, and the wear and tear that comes after many years of not being treated as a human person. One day, one of the prisoners sneaks into the loudspeaker room and plays a piece by Mozart over them. The characters stop in their tracks. The prisoners stare at the speakers, and it's as if you can see in their eyes the slow realization that they are actually human.

One of my recent addictions is *The Hunger Games*. I couldn't put them down, and I stayed up all night and finished them. It's a fictional story about a futuristic society that is Communist in nature, and they actually pit people against each other to fight to the death, and it's televised all over the country. And one of the things that ends up becoming a very important scene is that when one of the characters is killed, another person in the arena decorates her death with flowers and beauty. That become a pivotal moment in the entire trilogy. Why? Because it reminds people that they are human, and that they don't have to fight each other to the death.

## Beauty is not Subjective

![A urinal with the signature of Marcel Duchamp](/blog/fountain.webp)

The "Gospel" of today's society, if you will, is always telling us that beauty is in the eye of the beholder, and we're not supposed to question that, it's just how it is. But surely a kid scratching out a Metallica song on his electric guitar in his mom's basement is not of the same quality of the *Ave Maria* by Schubert, or Mozart, or Bach.

A few years ago downtown we had to fund an art project that was a couple of a million dollars featuring black and gnarled leather that had been cut up and was sitting on a post. And you may remember a few years earlier when our downtown art project was women on an electronic screen walking in place. A recent art show in New York featured a bed with ruffled sheets stained with blood, empty bottles of Vodka, and so on and so forth, showing debauchery. A group of artists recently got together and voted on the most influential piece of art in the last century, and what won was a piece called "The Fountain", a urinal that had been signed by the artist.

**Beauty is not in the eye of the beholder. Some things are more beautiful than others.** And it is the **stupidity** that some would call a bloody bed or a urinal "art" that kept people like me away from art for so many years and away from beauty.

## Beautiful People

![Saint Peter's Basilica](/blog/basilica.webp)

Now what in the world does any of this have to do with today? There are two levels: Firstly, We are called to produce and make things beautiful in our environment. Maybe that's a lesson plan that we're doing, or maybe it's an art project, or any number of other things: We're called to bring beauty into the world.

Secondly, we ourselves are to become beautiful. How do we do that? We do that by living a beautiful life. By living a life that is not tarnished by sin. The less sin that is in our life, the more beautiful we become. And we ought to realize that if we don't stand in God's path and throw sin and obstacles in his way, then he will continue to chisel, mold and sculpt us as we grow older slowly working out our imperfections and continuing to make us more beautiful.

We have a part to play in that: Are we going to confession? Are we receiving the Eucharist?

Are we reading? Are we writing? Are we immersing ourselves in scripture? Are we engaging in real, authentic relationships, not based on using the other person, but based on letting the other person help us become more beautiful?

Many people talk about how merely standing in the doorway at Saint Peter's Basilica in Rome or simply hearing a beautiful piece of music, or staring at a beautiful statue like Michelangelo's *Pieta* has caused thousands and thousands of people to return to God. If that's the case, if pieces of rock and architecture can do that, how much more can beautiful people call people back to Christ?

That's why we give thanks today for Mary and her sinless beauty. Her beauty, if we look for it, continues to inspire and lead people to her son, who is beauty in its truest sense. So I challenge you to make beautiful videos, beautiful pottery, cook beautiful food, sing beautiful songs, write beautiful poems and stories, draw and paint things that are beautiful, make all around you more beautiful. But most importantly, I challenge you to be beauty; through a life of virtue, that is free from the stain of sin.

**Mary, Queen of Beauty. Pray that we may share in you and your son's beauty this day.**
