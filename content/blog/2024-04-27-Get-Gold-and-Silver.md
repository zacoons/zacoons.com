---
title: Get Gold and Silver
date: 2024-04-27
---

First I will list some reasons who you shouldn't get precious metals.

1. It might be banned

# It's Not Archaic

I used to think gold and silver were archaic, but in fact we only abandoned the gold standard in 1944. When I say "we" I mean the US, even though I'm Australian, because Australia sucks up to the US. So we abandoned the gold standard in 1944 with the Bretton Woods agreement which prevented domestic convertibility of gold. This meant that people could no longer go to the bank end exchange their $20.67 for an ounce of gold. It meant that $20.67 was worth what the government said it was worth. Hence the name fiat money, because the word "fiat" is Latin for _an arbitrary order issued by a government or other authoritative figure_.

Anyway, what's this idea that archaic things are bad anyway? If a currency has stood the test of time as well as gold and silver, is that not a perfect demonstration of its great worth as a currency? Inversely, if we look at the history of fiat money, it has demonstrated complete incompetence at maintaining value.

# Why We Went Off the Gold Standard

From my brief search online it seems that people believe the gold standard is unstable, which I find very amusing.

According to [this article](https://www.stlouisfed.org/open-vault/2017/november/why-us-no-longer-follows-gold-standard) we shouldn't go back on the gold standard because:

- It doesn't guarantee financial or economic stability.
- It's costly and environmentally damaging to mine.
- The supply of gold is not fixed.

Let me explain why this makes me laugh. The supply of gold is far more fixed than the supply of fiat money. Gold is in fact very costly and difficult to mine, and it will only get more costly and difficult as we mine more of it, thus diminishing the supply. In contrast, the effort to print a $100 note is miniscule. Just flick a switch on the printing machine and you'll have thousands of them flying out at you in seconds. Now let me ask you, which one do you think is more economically stable?

The real reason we went off the gold standard is to give the government more control over our money. But they don't seem to be doing a very good job, so let's take back some of our control. Of course, some people aren't going to like that we are safe in our metal money armour, so they may try to strip it from us by having precious metals banned.

# Why You Should Get Gold and Silver

The rate of inflation is going bonkers. We've seen housing prices shoot through the roof, and now it's starting to have more of an effect on food. This is not because greedy big business is trying to steal your money, it's because your money is becoming less valuable. If you want to keep the money you earn, put it into gold or silver. Now.
