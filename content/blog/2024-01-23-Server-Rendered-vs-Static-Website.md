---
title: Server Rendered vs Static Websites
date: 2024-01-23
---

This website started out as an Apache + PHP combo, then I switched to serving pages using a Python server, but now I'm going pure static.

### Why Server Rendered

I started out making it server rendered because:

- I thought it was more efficient than a static site because I could just store the inner content for each page, then get the server to handle boilerplate (e.g. head tags, nav, footer...)
- The file structure was nicer.

### Why I Went to Static

Turns out serving static files is way faster and space efficient.

Page load speed (including response time)<br>
\* Static: 0.04s<br>
\* Server: 0.37s _~9x SLOWER_

Dir size (including deps)<br>
\* Static: 512 KB<br>
\* Server: 38.4 MB _~77x LARGER_

Big difference.

### The Best of Both Worlds

Instead of having to deal with an icky file structure, I've kept what I had before, except I replaced the Python server with a Python compiler script.

This script renders each page and places it in a bin/ directory. In a sense, it does what the server did, but instead of doing it for each request in realtime, it does it once, resulting in an entirely static website.

### Downside

The only downside is that this is a bit more manual work to update the site. For example, to add a blog post I would need to recompile and then upload the generated static content to the host machine. But hey, there are always tradeoffs.
