---
title: Say No to Self-Deprecating Humor
date: 2024-09-21
---

*Copied from [Denshi](https://denshi.org/blog/say-no-to-self-deprecating-humor)*

The second most recent generation, nicknamed *the Millenials*, is known for its distinct usage of self-deprecating humor. Whereas in previous generations it wasn't as socially acceptable to talk about the shortcomings of one's life (even rather serious and tragic ones) in such a humorous tone, this generation has been the first to openly practice this and even encourage it. You can see it all over media, the internet and even in your own friend group. Mine certainly does this a lot, and I too definitely used to engage in those conversations. In fact, you may have noticed me making self-deprecating jokes in some of my older videos as well.

Now, being more open about one's own flaws is not necessarily wrong; It can be a powerful way of expressing humility, and it can be amusing if it's relatable. After all, we aren't always at our 100%, and nobody likes a boaster! ***However***, I write this article to warn people against using self-deprecating humor not because it's wrong to be funny or realistic, but because **it's not just self-deprecating, it can be self-ruining**.

## It's no longer about "humility"

Getting straight to the point: Excessive exposure to this humor spreads a very specific type of *pessimism* which may be used as a deterrent against productive thought. *In the way it's used now*, self-deprecating jokes aren't a form of humility. Instead, it's a form of dismissal; We are dismissing the fact that there is more to aspire to in life.

What's even more interesting is that often, the joke being made isn't true, and to a very high extent. Take someone who constantly "jokes" about how "bad at math" they are: It is telling that many people I've known personally who were perfectly reasonable at the subject still found the need to self-deprecate about their supposed lack of skills, *often a lot more than those who actually needed help*. This humor is almost a psychological virus, which on the surface-level appears perfectly innocent, but which hides more dangerous undertones. At the end of the day, it's a cry for attention.

## It drags us down

Some of the best advice I've ever been told ([told to me by a peculiar man](https://www.youtube.com/@Lio_Convoy)) is to ***surround yourself with people you wanna be like***. You are who you are, but who you are is a collection of experiences, opinions and habits observed around other people who are (hopefully) good influences. I found that, as my friends kept making such self-deprecating statements, I too started feeling genuinely less secure about my own abilities, even when nothing had happened to change them.

## Conclusion

Many view self-deprecating humor as a symptom of a wider problem. As they say, "it's funny ‘cause it's true!". To them, self-deprecating humor simply "represents" the contemporary attitude towards life and the human experience. ***I really don't like this!*** It's one thing to have everyday struggles, another to normalize a type of humor that acts as a way for people to adjust (or "cope") to a worse reality.

So, **yes!** From now on, I will try my best to limit excessive self-deprecating humor from my videos and streams. Not because life is always great and positive, but exactly because I don't want to add to the negativity.
