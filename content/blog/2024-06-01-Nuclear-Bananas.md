---
title: Nuclear Bananas
date: 2024-06-01
---

*Inspired by XKCD's [Hot Bananas](https://what-if.xkcd.com/158)*

In theory, you can make a nuclear reactor with anything that is radioactive, right? Bananas are radioactive...

Bananas are radioactive because: pretty much everything is a bit radioactive, even a brick, and; bananas contain potassium, some of which is the radioactive isotope potassium-40. But don't worry, a bit of radiation won't hurt you. In fact, bananas aren't even very radioactive in the broad scope of things. About 15 atoms decay every second in a banana,<sup>[1](https://en.wikipedia.org/wiki/Banana_equivalent_dose)</sup> whereas your body is producing about 4,000 decays per second.<sup>[2](http://hps.org/publicinformation/ate/faqs/faqradbods.html)</sup> And if you ever want to know how much radiation comes from your pizza, you can look it up on Google [[?]](https://www.google.com/search?q=pizza+potassium) (1 g of potassium has ~31 decays/s).

Radioactive decay happens when an atom is too big for itself, and as a result of being so fat it can't hold all of itself together. So it sometimes shoots part of itself out, and turns into a different element. This is called a decay.

Atom → fatom → badoom

Each atom has a small chance of decaying at any given time. When one decays, the speedy projectile that was expelled barges into the atoms around it, jiggling them around, and creating heat energy. Nuclear reactors use this decay energy to heat up water very hot, which creates steam, which spins a turbine, which generates electricity<sup>[3](https://www.youtube.com/watch?v=IbK2FennCgY)</sup>. But for fuel they use uranium. Not bananas. How boring.

1 g of uranium-235 = 570 decays/s <sup>[4](https://ataridogdaze.com/science/uranium-decay-rate.html)</sup>\
1 g of banana = 0.125 decays/s

It turns out that the main source of energy in a nuclear reactor is atomic fission rather than radioactive decay. Decay yields a little heat, but not enough to be useful. It is the fission, caused by decay, which yields enough heat to be useful. Unfortunately potassium-40 is not fissile, so the only heat it releases is from radioactive decay. So we're going to need an outrageous quantity of bananas.

It takes 33,472 joules to boil a gram of water. Taking the decay energy of a potassium-40 atom to be 1.31 MeV (mega electronvolts),<sup>[5](https://en.wikipedia.org/wiki/Potassium-40)</sup> we would need over 100 trillion bananas in order to boil one single gram of water (in 1 second). Taking a banana to be 12 cm long and 5 cm wide, that's more than enough bananas to cover Madagascar [[?]](https://www.google.com/maps/place/Madagascar).<sup>[6](https://www.worldometers.info/geography/largest-countries-in-the-world)</sup> Alas, the worldwide production of bananas every year is only around 1.125 trillion bananas. Not only would it take 90 years to stockpile enough bananas, but we would also need an impressive feat of engineering to condense them all into a practical volume. And all of that just to boil one single tiny gram of water.

So with my extensive research I have concluded it is not possible to make a banana nuclear reactor :(
