---
title: Don't Be Afraid of AI
date: 2023-11-23
---

> "There is no fear in love" - 1 John 4:18

AI is rather scary, but do not fear, for God is with us.

I love my job. I love to code. I would be sad if I couldn't code. So I can sympathise with people who are afraid of losing their jobs. But that's not why AI is scary.

### Evil Uses of AI

Nor should you fear AI taking over the world. It's not going to take over the world. It's not smart enough to take over the world. It doesn't have a conscience so it's not going to revolt against us. We can't even predict the weather next week let alone create a living being. Thou canst not make one hair white or black.

AI may be (and is) used for scams, identity theft, probably generating porn, and a multitude of other evil things. But all technology is used for evil. All technology is used for evil and for good. It's impossible for us to stop this. Look back at the War on Drugs. Was it a victory, or a massive waste of resources? AI will be used to do evil, but so what? Guns are used to do evil, yet every patriotic American knows why gun rights should be upheld. Likewise, the use and creation AI should be upheld.

### Fearmongering

The main reason why AI is scary is because people think it's scary. Be not afraid. When people are afraid, they have a tendency to relinquish freedom. Just look at the recent COVID susdemic where people covered themselves with masks and "self isolated" for many months because the gubment said to. Or the War on Terrorism and the subsequent Patriot Act which allowed the American government to collect massive amounts of data on its subjects. Or Pearl Harbour...

Anyway, if enough of us let fear get to our heads, we will soon find legislation appearing around the use and creation of AI. Then we will find legislation popping up around the use of the internet - the last bastion of true freedom. It is important to defend the internet. It is vitally important. We don't need digital ID. We don't need a digital passport to defend us from identity theft. Do not fear.

Don't be afraid of not knowing whether something or someone is real or AI generated. There are two reasons for this. Firstly, AI needs to be hosted on a server. Which means anyone with the malicious intent of making fake AI humans must pay for it. And if they want to saturate the internet with these fake AI humans, they must have a lot of money. Secondly, geeky nerds will probably find a solution to the AI identification problem on their own. Just like they have solved other security problems throughout internet history. Without needing legislation.

### Economic Overlord

Some people believe we could perfect the economy by putting an AI in control. It could predict what people need and when they will need it, thus allowing for a perfect allocation of resources. But this would mean giving our decision making ability to a computer (or whoever owns the computer). It would mean giving our property to a computer (or whoever owns the computer). It would mean ignoring our preferences, because a computer can't possibly know our fleeting, ever changing whims. A world with AI in control of the economy would be world in the depths of a tyrannical socialism in which one small mistake could lead to millions of people starving to death, and there would be nobody to blame. The computer would be to blame. Just a glitch in the system. Heartless destruction at the overlord's fingertips, and nobody can blame him. That is pure power.

### Conclusion

AI will continue to be put to evil use, but it is the same with all technology. What's more important is to not let fear take hold of us. AI will be used to make us afraid, and if we let it, it will lead us to relinquish our freedom. It may even be used to "plan" the economy and give incredible power to the metaphorical man behind the curtain. Even so, we musn't be afraid. If we all seek to build communities of brotherly love and faithful courage, we will triumph.
