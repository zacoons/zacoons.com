---
title: Why You Should Start a Blog
date: 2023-12-28
---

There are two main reasons.

Firstly, it helps you flesh out ideas. At least it does for me. Furthermore, if other people disagree with your post, you're forced to defend those ideas, which helps solidify them in your head. Well either that, or your ideas are wrong, and the other person convinces you to the right. Stay humble!

Secondly, it spreads your ideas to other people. If you're going to the effort of writing a post about it, it's probably worth sharing. Go make disciples!

### HmMmMMMMmmMMmMm (thinking noise)

Sometimes I have an idea that makes perfect sense in my head. Then, when I go try to explain it to someone, it all falls apart. Writing does the same thing, except you don't need someone to listen to you blabbering about nonsense.

I have found writing these little posts quite valuable. It helps me think through stuff and question things I've never questioned before. When I started this blog, I was so enthusiastic that I would think of ways to make a fascinating post about any small thing. That enthusiasm has worn off a little, but I aspire to regain it, because I think that's how people should be. Finding wonder in even the smallest thing. I want you to share that joy.

### Not an Essay

I had to write a lot of essays for school, and while they were valuable in teaching me the skill of writing, these posts are different. I don't have a deadline, I don't have a word limit, and I don't have an arbitrary topic. I can just write about what's on my mind, or about cool things that I want to share with other people.

### The Spread of Ideas

Humans were made to live in communion with one another. We can't thrive by going it alone. It's fun being able to host events, it's nice to have people you can ask for help, and it's good to have neighbours who keep you on the straight and narrow. When you start getting out of line, good neighbours will let you know, and maybe lend a hand.

"Neighbours" means a lot more than just your next door neighbour. By "neighbour" I mean anyone in the same community as you. That might be an online forum community, a Matrix community, a Discord community, an Odysee community, a Youtube community, a boy scout community, a girls brigade community, a knitting community, a church community, a business community, or anything other kind of community.

### Why a Blog and Not a Youtube Channel?

Starting a blog is one step towards building your own community.

"Sure," you might say, "one tiny eensie weensie step... Because nobody reads blog anymore. lol XD"

That isn't entirely true though, there are still some eccentrics, like me, who read blog posts now and again. And eccentric people are more likely to be good people. At least, more likely than your average midwit. True, you may attract more people on youtube, but are those the types of people you really want to build a community with? That's a genuine question, not a judgement. Be aware of the audience you're catering to, and make sure they're the sorts of people you want to hang out with.

### Conclusion

Just because you have a blog doesn't mean you need to post on a schedule or stay within the domain of a certain topic. In fact, focusing too much on these things can be unhealthy.

Anyway, start a blog. I made a tutorial on how to code your own which you can find [here](https://odysee.com/blog-tuts). But there are plenty of other tutorials out there. Or just use Wordpress or something like that. But with your own website, you can do much more than just blogging...
